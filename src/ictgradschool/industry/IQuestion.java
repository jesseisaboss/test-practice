package ictgradschool.industry;

public interface IQuestion {
    public void printQuestion();

    public boolean isValidAnswer(String userInput);

    public  boolean isCorrect(String userInput);
}
