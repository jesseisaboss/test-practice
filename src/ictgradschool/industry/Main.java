package ictgradschool.industry;

import java.security.Key;

public class Main {

//    9) Create a new class called "Main". Inside that class, make a static main method (Remember the "psvm" IntelliJ shortcut!), and a non-static void called "start()" (i.e. just like many of the lab exercises you've seen).

//        10) Create a new method in the Main class called createQuestions().
//                - This method should create a new IQuestion[] array, of length 5.
//                - The method should then populate the array with five MultiChoiceQuestion instances (the contents of which are up to you!).
//        - Finally, the method should return the array.

//        11) Add code to the start() method to complete the program. The code should do the following:
//        a) Call the createQuestions() method to get the array of questions to ask.
//
//                b) Loop through the questions array. For each question:
//
//        c) Ask the question to the user
//
//        d) Get the user's answer (use Keyboard class)
//
//        e) Check whether the user has entered a valid answer (i.e. that the question's isValidAnswer method returns true). If they haven't, prompt them to enter a new anser, and go back to (c).
//
//                f) Check whether the user has entered a correct answer (i.e. that the question's isCorrect method returns true). Print a message saying whether or not their answer was correct. If it was correct, add one to the user's score.
//
//        g) After the questions loop, print a message similar to "You answered 3 out of 5 questions correctly".

    public void start(){
        IQuestion[] newArray = createQuestions();
        int userScore = 0;
        for (int i = 0; i < newArray.length; i++) {
            newArray[i].printQuestion();

            String input = "";
            while(newArray[i].isValidAnswer(input)== false){
                System.out.print("What is your answer?: ");
                input = Keyboard.readInput();
            }
            if(newArray[i].isCorrect(input)){
                System.out.println("Correct");
                userScore++;
            }
        }
        System.out.println("You answered " + userScore + " out of 5 questions correctly.");
    }

    public IQuestion[] createQuestions(){
        IQuestion[] question = new IQuestion[5];
        question[0] = new MultiChoiceQuestion("What is the meaning of life?", new String[]{"yes", "no", "1", "fun"},"D");
        question[1] = new MultiChoiceQuestion("What is the meaning of class?", new String[]{"yes", "no", "1", "fun"},"A");
        question[2] = new MultiChoiceQuestion("What is the meaning of giraffe?", new String[]{"yes", "no", "1", "fun"},"B");
        question[3] = new MultiChoiceQuestion("What is the meaning of bob?", new String[]{"yes", "no", "1", "fun"},"C");
        question[4] = new MultiChoiceQuestion("What is the meaning of something?", new String[]{"yes", "no", "1", "fun"},"D");
        return question;
    }

    public static void main(String[] args) {
        Main something = new Main();
        something.start();
    }
}
