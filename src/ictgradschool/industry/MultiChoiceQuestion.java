package ictgradschool.industry;

import java.util.Arrays;

public class MultiChoiceQuestion implements IQuestion{
    public String question;
    public String[] possibleAnswers;
    public String correctAnswer;

    public MultiChoiceQuestion(String question, String[] possibleAnswers, String correctAnswer) {
        this.question = question;
        this.possibleAnswers = possibleAnswers;
        this.correctAnswer = correctAnswer;


    }



//    What is 1 + 1?
//    A) 0
//    B) 2
//    C) -7
//    D) Infinity
    @Override
    public void printQuestion() {
        System.out.println(question);
        char letter = 'A';
        for (int i = 0; i <possibleAnswers.length ; i++) {
            System.out.print(letter + ") ");
            System.out.println(possibleAnswers[i]);
            letter ++;
        }
    }
//8) Implement the isValidAnswer() method for MultiChoiceQuestion. This should return true if the supplied answer string is any of the possible answer letters (ignore case)
//   - Continuing from the previous example, the method should return true if the supplied answer is "A", "B", "C", or "D", or any of the lowercase equivalents. Otherwise it should return false.
//
//    HINT:
//    char letter = 'C';
//if (letter >= 'A' && letter <= 'D') {
//        System.out.println("Hello"); // This message will be printed.
//    }
    @Override

    public boolean isValidAnswer(String userInput) {
        if(userInput.isEmpty()){
            return false;
        }
        char letter = (userInput.toUpperCase()).charAt(0);
        if (letter >= 'A' && letter <= 'D') {
            System.out.println("");
            return true;
        }
        return false;
    }
//7) Implement the isCorrect() method for MultiChoiceQuestion. This should return true if the supplied answer string is the correct letter (ignore case).
//            - Continuing from the previous example, the method should return true if the supplied answer is "B" or "b". Otherwise it should return false.
    @Override
    public boolean isCorrect(String userInput) {
        return (userInput.toUpperCase()).equals(correctAnswer);
    }

}

